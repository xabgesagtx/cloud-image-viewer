package com.canceledsystems.cloudimageviewer.gallery

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

internal class ThumbnailServiceTest {

    private val service = ThumbnailService(ThumbnailProperties())

    @Test
    fun `calcDimensions takes scales up to max values`() {
        assertThat(service.calcDimensions(1, 1, 2, 2), equalTo(Pair(2, 2)))
    }

    @Test
    fun `calcDimensions scales down because width exceeds maxWidth`() {
        assertThat(service.calcDimensions(10, 2, 5, 4), equalTo(Pair(5, 1)))
    }

    @Test
    fun `calcDimensions scales down because height exceeds maxheight`() {
        assertThat(service.calcDimensions(2, 10, 4, 5), equalTo(Pair(1, 5)))
    }

    @Test
    fun `calcDimensions scales down when both exceed max values`() {
        assertThat(service.calcDimensions(6, 10, 5, 5), equalTo(Pair(3, 5)))
        assertThat(service.calcDimensions(10, 6, 5, 5), equalTo(Pair(5, 3)))
    }

}