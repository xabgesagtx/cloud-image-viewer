package com.canceledsystems.cloudimageviewer.gallery

import com.canceledsystems.cloudimageviewer.files.FileData
import com.canceledsystems.cloudimageviewer.files.FileInfoImpl
import com.canceledsystems.cloudimageviewer.files.FileRepository
import com.nhaarman.mockitokotlin2.*
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.notNullValue
import org.hamcrest.Matchers.nullValue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class GalleryServiceTest {

    @InjectMocks
    lateinit var service: GalleryService

    @Mock
    lateinit var repo: FileRepository

    @Mock
    lateinit var thumbnailService: ThumbnailService

    private val path = "test/"
    private val subFolder = FileInfoImpl("test/sub-folder/", "", true)
    private val image1 = FileInfoImpl("test/image1.jpg", "image/jpg", false)
    private val image2 = FileInfoImpl("test/image2.png", "image/png", false)
    private val thumbnailPath1 = "test/thumbs/image1.jpg"
    private val thumbnailPath2 = "test/thumbs/image2.png"

    @Test
    fun `list returns gallery listing`() {
        mockListing()

        val actual = service.list(path)
        val expectedRoot = GalleryFolder("..", "")
        val expectedSubFolder = GalleryFolder(subFolder.name, subFolder.fullPath)
        val expectedImage1 = GalleryImage(image1.name, image1.fullPath, thumbnailPath1)
        val expectedImage2 = GalleryImage(image2.name, image2.fullPath, thumbnailPath2)
        val expected = GalleryListing(listOf(expectedRoot, expectedSubFolder), listOf(expectedImage1, expectedImage2))
        assertThat(actual, equalTo(expected))
    }

    @Test
    fun `getImageData returns null if file could not be found`() {
        val imagePath = "image.jpg"
        whenever(repo.getData(imagePath)).thenReturn(null)
        assertThat(service.getImageData(imagePath), nullValue())
    }

    @Test
    fun `getImageData returns null if file is not an image`() {
        val imagePath = "image.html"
        whenever(repo.getData(imagePath)).thenReturn(FileData(imagePath, "text/html", 0, ByteArray(0).inputStream()))

        assertThat(service.getImageData(imagePath), nullValue())
    }

    @Test
    fun `getImageData returns image`() {
        val imagePath = "image.jpg"
        val mimeType = "image/jpg"
        whenever(repo.getData(imagePath)).thenReturn(FileData(imagePath, mimeType, 0, ByteArray(0).inputStream()))

        val actual = service.getImageData(imagePath)
        assertThat(actual, notNullValue())
        assertThat(actual?.fullPath, equalTo(imagePath))
        assertThat(actual?.mimeType, equalTo(mimeType))
    }

    @Test
    fun `createThumbnails without regenerating existing thumbnails`() {
        mockListing()
        whenever(thumbnailService.generate(any())).then { ByteArray(0) }
        whenever(repo.exists(thumbnailPath1)).thenReturn(true)
        whenever(repo.exists(thumbnailPath2)).thenReturn(false)
        whenever(repo.getData(image2.fullPath)).thenReturn(FileData(image2.fullPath, image2.mimeType, 0, ByteArray(0).inputStream()))

        val actual = service.createThumbnails(path, false)
        assertThat(actual, equalTo(1))
        verify(repo).list(path)
        verify(repo).getData(image2.fullPath)
        verify(repo).exists(thumbnailPath1)
        verify(repo).exists(thumbnailPath2)
        verify(thumbnailService).generate(argThat { name == image2.name })
        verify(repo).save(eq(thumbnailPath2), any(), eq(image2.mimeType))
    }

    @Test
    fun `createThumbnails with regenerating existing thumbnails`() {
        mockListing()
        whenever(thumbnailService.generate(any())).then { ByteArray(0) }
        whenever(repo.getData(image1.fullPath)).thenReturn(FileData(image1.fullPath, image1.mimeType, 0, ByteArray(0).inputStream()))
        whenever(repo.getData(image2.fullPath)).thenReturn(FileData(image2.fullPath, image2.mimeType, 0, ByteArray(0).inputStream()))

        val actual = service.createThumbnails(path, true)
        assertThat(actual, equalTo(2))
        verify(repo).list(path)
        verify(repo).getData(image1.fullPath)
        verify(repo).getData(image2.fullPath)
        verify(repo, never()).exists(any())
        verify(thumbnailService).generate(argThat { name == image1.name })
        verify(thumbnailService).generate(argThat { name == image2.name })
        verify(repo).save(eq(thumbnailPath1), any(), eq(image1.mimeType))
        verify(repo).save(eq(thumbnailPath2), any(), eq(image2.mimeType))
    }

    fun mockListing() {
        val fileToIgnore = FileInfoImpl("test/readme.html", "text/html", false)
        val fileInfos = listOf(subFolder, image1, image2, fileToIgnore)
        whenever(repo.list(path)).thenReturn(fileInfos)
    }
}