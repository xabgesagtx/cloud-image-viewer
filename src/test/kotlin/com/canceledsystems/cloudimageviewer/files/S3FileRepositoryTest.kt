package com.canceledsystems.cloudimageviewer.files

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.containers.localstack.LocalStackContainer.Service.S3
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.CreateBucketRequest
import software.amazon.awssdk.services.s3.model.HeadBucketRequest
import software.amazon.awssdk.services.s3.model.NoSuchBucketException
import software.amazon.awssdk.services.s3.model.PutObjectRequest

@Testcontainers
internal class S3FileRepositoryTest {

    companion object {
        @Container
        @JvmStatic
        private val localstack = LocalStackContainer(DockerImageName.parse("localstack/localstack:0.12.4"))
                .withServices(S3)
    }

    private lateinit var repo: FileRepository
    private lateinit var s3: S3Client

    private val bucket = "image-bucket"
    private val imagePath1 = "test/image1.jpg"
    private val imagePath2 = "test/image2.jpg"
    private val imagePath3 = "test/sub-folder/image3.jpg"
    private val imageMimeType = "image/jpg"

    @BeforeEach
    fun setUp() {
        s3 = S3Client.builder()
            .endpointOverride(localstack.getEndpointOverride(S3))
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create(
                localstack.accessKey, localstack.secretKey
            )))
            .region(Region.of(localstack.region))
            .build()

        createTestData()

        val properties = S3Properties()
        properties.bucket = bucket
        repo = S3FileRepository(s3, properties)
    }

    private fun createTestData() {
        try {
            val headBucketRequest = HeadBucketRequest.builder()
                .bucket(bucket)
                .build()
            s3.headBucket(headBucketRequest)
        } catch (e: NoSuchBucketException) {
            val createBucketRequest = CreateBucketRequest.builder()
                .bucket(bucket)
                .build()
            s3.createBucket(createBucketRequest)
            uploadImage(imagePath1)
            uploadImage(imagePath2)
            uploadImage(imagePath3)
        }
    }

    private fun uploadImage(path: String) {
        val request = PutObjectRequest.builder()
            .key(path)
            .contentType(imageMimeType)
            .bucket(bucket)
            .build()
        val requestBody = RequestBody.fromBytes(ByteArray(0))
        s3.putObject(request, requestBody)
    }

    @Test
    fun `save and get`() {
        val newImagePath = "new/newImage.png"
        repo.save(newImagePath, ByteArray(0), "image/png")
        val fileData = repo.getData(newImagePath)
        assertThat(fileData, notNullValue())
        assertThat(fileData?.fullPath, equalTo(newImagePath))
        assertThat(fileData?.name, equalTo("newImage.png"))
        assertThat(fileData?.mimeType, equalTo("image/png"))
    }

    @Test
    fun `exists returns true for existing file`() {
        assertTrue(repo.exists(imagePath1))
    }

    @Test
    fun `exists returns false for non existing file`() {
        assertFalse(repo.exists("non-existing.png"))
    }

    @Test
    fun `getData returns null for non existing file`() {
        assertNull(repo.getData("non-existing.png"))
    }

    @Test
    fun list() {
        val actual = repo.list("test/")
        val folder = FileInfoImpl("test/sub-folder/", "unknown", true)
        val image1 = FileInfoImpl(imagePath1, imageMimeType, false)
        val image2 = FileInfoImpl(imagePath2, imageMimeType, false)
        val expected: List<FileInfo> = listOf(folder, image1, image2)
        assertThat(actual, equalTo(expected))
    }
}