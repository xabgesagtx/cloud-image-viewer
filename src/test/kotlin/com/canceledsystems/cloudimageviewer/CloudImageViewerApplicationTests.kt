package com.canceledsystems.cloudimageviewer

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository

@SpringBootTest(properties = ["s3.key=key","s3.secret=secret","s3.bucket=bucket","s3.region=region","s3.endpoint=https://example.com"])
class CloudImageViewerApplicationTests {

	@MockBean
	lateinit var clientRepo: ClientRegistrationRepository

	@Test
	fun contextLoads() {
	}

}
