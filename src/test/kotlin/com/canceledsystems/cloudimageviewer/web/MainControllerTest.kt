package com.canceledsystems.cloudimageviewer.web

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@WebMvcTest(controllers = [MainController::class], includeFilters = [ComponentScan.Filter(classes = [ConfigurationProperties::class])])
@Import(ThymeleafConfig::class, Auth0LogoutHandler::class)
internal class MainControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var clientRegistrationRepository: ClientRegistrationRepository

    @Test
    fun loggedOut() {
        mockMvc.get("/")
                .andExpect {
                    status { isOk() }
                    content { contentType("text/html;charset=UTF-8")}
                    xpath("//section/p/a") {
                        string("login")
                    }
                }
    }

    @Test
    @WithMockUser(value = "user")
    fun loggedIn() {
        mockMvc.get("/")
                .andExpect {
                    status { isOk() }
                    content { contentType("text/html;charset=UTF-8")}
                    xpath("//section/p/a") {
                        string("Explore the galleries in your cloud")
                    }
                }
    }
}
