package com.canceledsystems.cloudimageviewer.web

import com.canceledsystems.cloudimageviewer.gallery.*
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@WebMvcTest(controllers = [GalleryController::class], includeFilters = [ComponentScan.Filter(classes = [ConfigurationProperties::class])])
@Import(ThymeleafConfig::class, Auth0LogoutHandler::class)
internal class GalleryControllerTest {

    @MockBean
    lateinit var service: GalleryService

    @MockBean
    lateinit var clientRepository: ClientRegistrationRepository

    @Autowired
    lateinit var mockMvc: MockMvc

    private val path = "test/"

    @Test
    @WithMockUser(authorities = ["SCOPE_cloudimageviewer"])
    fun list() {
        val folder1 = GalleryFolder("folder1", "folderPath1")
        val folder2 = GalleryFolder("folder2", "folderPath2")
        val folders = listOf(folder1, folder2)
        val image1 = GalleryImage("image1.jpg", "imagePath1", "thumbnailPath1")
        val image2 = GalleryImage("image2.jpg", "imagePath2", "thumbnailPath2")
        val images = listOf(image1, image2)
        val listing = GalleryListing(folders, images)
        whenever(service.list(path)).thenReturn(listing)

        mockMvc.get("/gallery") {
            param("path", path)
        }.andExpect {
            status {
                isOk()
            }
            content {
                contentType("text/html;charset=UTF-8")
            }
            xpath("//h2") {
                string(path)
            }
            xpath("//section/ul/li") {
                nodeCount(2)
            }
            xpath("normalize-space(//section/ul/li[1])") {
                string(folder1.name)
            }
            xpath("normalize-space(//section/ul/li[2])") {
                string(folder2.name)
            }
            xpath("//*[@id='image-gallery']/a") {
                nodeCount(2)
            }
            xpath("//*[@id='image-gallery']/a[1]/@title") {
                string(image1.name)
            }
            xpath("//*[@id='image-gallery']/a[2]/@title") {
                string(image2.name)
            }
        }
    }

    @Test
    @WithMockUser
    fun `list is forbidden for user without appropriate authorities`() {
        mockMvc.get("/gallery") {
            param("path", path)
            with(csrf())
        }.andExpect {
            status {
                isForbidden()
            }
        }
    }

    @Test
    fun `list forwards when not logged in`() {
        mockMvc.get("/gallery") {
            param("path", path)
            with(csrf())
        }.andExpect {
            status {
                isFound()
            }
        }
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_cloudimageviewer"])
    fun createThumbnails() {
        whenever(service.createThumbnails(path, false)).thenReturn(1)

        mockMvc.post("/gallery/thumbnails") {
            param("path", path)
            with(csrf())
        }.andExpect {
            status {
                isOk()
            }
            content {
                string("1")
                contentType(MediaType.APPLICATION_JSON)
            }
        }
    }

    @Test
    fun `createThumbnails forwards when not logged in`() {
        mockMvc.post("/gallery/thumbnails") {
            param("path", path)
            with(csrf())
        }.andExpect {
            status {
                isFound()
            }
        }
    }

    @Test
    @WithMockUser(authorities = ["SCOPE_cloudimageviewer"])
    fun getImage() {
        val imagePath = "image.jpeg"
        val mimeType = "image/jpg"
        whenever(service.getImageData(imagePath)).thenReturn(ImageData(imagePath, mimeType, 0, ByteArray(0).inputStream()))
        mockMvc.get("/gallery/image") {
            param("path", imagePath)
        }.andExpect {
            status {
                isOk()
            }
            content {
                contentType(mimeType)
            }
        }
    }

    @Test
    fun `getImage forwards when not logged in`() {
        mockMvc.get("/gallery/image") {
            param("path", path)
            with(csrf())
        }.andExpect {
            status {
                isFound()
            }
        }
    }
}