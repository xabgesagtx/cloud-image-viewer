package com.canceledsystems.cloudimageviewer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CloudImageViewerApplication

fun main(args: Array<String>) {
	runApplication<CloudImageViewerApplication>(*args)
}
