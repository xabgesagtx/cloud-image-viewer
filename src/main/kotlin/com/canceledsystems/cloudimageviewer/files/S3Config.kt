package com.canceledsystems.cloudimageviewer.files

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import java.net.URI

@Configuration
class S3Config {

    @Bean
    fun s3Client(properties: S3Properties): S3Client =
        S3Client.builder()
            .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(properties.key, properties.secret)))
            .endpointOverride(URI.create(properties.endpoint))
            .region(Region.of(properties.region))
            .build()

}