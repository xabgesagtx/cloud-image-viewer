package com.canceledsystems.cloudimageviewer.files

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_GATEWAY)
class S3Exception(message: String, cause: Throwable): RuntimeException(message, cause)