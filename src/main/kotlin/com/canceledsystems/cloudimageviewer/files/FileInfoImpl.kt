package com.canceledsystems.cloudimageviewer.files

data class FileInfoImpl(override val fullPath: String,
                        override val mimeType: String,
                        override val directory: Boolean) : FileInfo