package com.canceledsystems.cloudimageviewer.files

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "s3")
class S3Properties {
    lateinit var key: String
    lateinit var secret: String
    lateinit var bucket: String
    lateinit var region: String
    lateinit var endpoint: String
}