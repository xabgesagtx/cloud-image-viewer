package com.canceledsystems.cloudimageviewer.files

interface FileRepository {

    fun save(fullPath: String, data: ByteArray, mimeType: String)

    fun getData(fullPath: String): FileData?

    fun list(path: String): List<FileInfo>

    fun exists(fullPath: String): Boolean

}