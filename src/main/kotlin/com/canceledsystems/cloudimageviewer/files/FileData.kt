package com.canceledsystems.cloudimageviewer.files

import java.io.InputStream

class FileData(override val fullPath: String,
               override val mimeType: String,
               val size: Long,
               val data: InputStream): FileInfo {
    override val directory = false
}