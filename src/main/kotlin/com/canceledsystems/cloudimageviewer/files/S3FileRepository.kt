package com.canceledsystems.cloudimageviewer.files

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import software.amazon.awssdk.awscore.exception.AwsServiceException
import software.amazon.awssdk.core.exception.SdkClientException
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*


@Repository
internal class S3FileRepository(private val client: S3Client,
                       private val properties: S3Properties) : FileRepository {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun save(fullPath: String, data: ByteArray, mimeType: String) {
        log.info("Saving $fullPath")
        try {
            val request = PutObjectRequest.builder()
                .key(fullPath)
                .contentType(mimeType)
                .contentLength(data.size.toLong())
                .bucket(properties.bucket)
                .build()
            val requestBody = RequestBody.fromBytes(data)
            client.putObject(request, requestBody)
        } catch (e: AwsServiceException) {
            log.info("Failed to upload $fullPath (status: ${e.statusCode()}, error: ${e.message})")
        } catch (e: SdkClientException) {
            throw S3Exception("Failed to upload $fullPath", e)
        }
    }

    override fun exists(fullPath: String): Boolean {
        return try {
            val request = HeadObjectRequest.builder()
                .key(fullPath)
                .bucket(properties.bucket)
                .build()
            client.headObject(request)
            true
        } catch (e: NoSuchKeyException) {
            false
        } catch (e: SdkClientException) {
            throw S3Exception("Failed to check if $fullPath exists", e)
        }
    }

    override fun getData(fullPath: String): FileData? {
        log.info("Retrieving $fullPath")
        return try {
            val request = GetObjectRequest.builder()
                .bucket(properties.bucket)
                .key(fullPath)
                .build()
            val inputStream = client.getObject(request)
            val response = inputStream.response()
            FileData(fullPath, response.contentType(), response.contentLength(), inputStream)
        } catch (e: AwsServiceException) {
            if (e.statusCode() == 404) {
                null
            } else {
                throw S3Exception("Failed to retrieve $fullPath", e)
            }
        } catch (e: SdkClientException) {
            throw S3Exception("Failed to retrieve $fullPath", e)
        }
    }

    override fun list(path: String): List<FileInfo> {
        log.info("Listing directory $path")
        val result = mutableListOf<FileInfo>()
        try {
            var req = ListObjectsV2Request.builder()
                .bucket(properties.bucket)
                .prefix(path)
                .delimiter("/")
                .build()
            var objectListing: ListObjectsV2Response
            do {
                objectListing = client.listObjectsV2(req)
                objectListing.commonPrefixes().map { FileInfoImpl(it.prefix(), "unknown", true) }
                        .forEach { result.add(it) }
                objectListing.contents()
                        .filter { it.key() != path }
                        .map { FileInfoImpl(it.key(), it.key().mimeType, it.key().endsWith("/")) }
                        .forEach { result.add(it) }
                val token = objectListing.nextContinuationToken()
                req = req.toBuilder()
                    .continuationToken(token)
                    .build()
            } while (objectListing.isTruncated)
        } catch (e: SdkClientException) {
            throw S3Exception("Could not list objects in $path", e)
        }
        return result
    }
}

private val String.mimeType: String
    get() {
        val suffix = substringAfterLast(".").toLowerCase()
        return if (suffix in arrayOf("jpeg", "jpg", "png")) {
            "image/$suffix"
        } else {
            "unsupported"
        }
    }