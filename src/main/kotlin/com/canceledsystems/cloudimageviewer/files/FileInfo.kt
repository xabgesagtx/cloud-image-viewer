package com.canceledsystems.cloudimageviewer.files

interface FileInfo {
    val fullPath: String
    val mimeType: String
    val directory: Boolean
    val image: Boolean
        get() {
            return !directory && mimeType.startsWith("image/")
        }
    val basePath: String
        get() {
            return if (trimmedPath.contains("/")) {
                trimmedPath.substringBeforeLast("/")
            } else {
                ""
            }
        }
    val name: String
        get() {
            return trimmedPath.substringAfterLast("/")
        }
    private val trimmedPath: String
        get() {
            return fullPath.removeSuffix("/")
        }

}