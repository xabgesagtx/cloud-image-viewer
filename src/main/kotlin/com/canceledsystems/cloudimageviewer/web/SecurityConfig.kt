package com.canceledsystems.cloudimageviewer.web

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter


@Configuration
internal class SecurityConfig(private val logoutHandler: Auth0LogoutHandler) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
                .antMatchers("/gallery/**")
                .hasAuthority("SCOPE_cloudimageviewer")
                .anyRequest()
                .permitAll()
            .and()
                .logout()
                .logoutUrl("/logout").permitAll()
                .addLogoutHandler(logoutHandler)
            .and()
                .oauth2Client()
            .and()
                .oauth2Login()
    }
}