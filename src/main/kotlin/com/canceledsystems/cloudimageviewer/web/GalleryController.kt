package com.canceledsystems.cloudimageviewer.web

import com.canceledsystems.cloudimageviewer.gallery.GalleryService
import com.canceledsystems.cloudimageviewer.gallery.ImageData
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.ModelAndView

@RestController
@RequestMapping("gallery")
class GalleryController(private val service: GalleryService) {

    @GetMapping("")
    fun list(@RequestParam(value = "path", defaultValue = "") path: String, modelAndView: ModelAndView): ModelAndView {
        modelAndView.viewName = "gallery"
        val listing = service.list(path)
        modelAndView.addObject("path", path)
        modelAndView.addObject("images", listing.images)
        modelAndView.addObject("folders", listing.folders)
        return modelAndView
    }

    @PostMapping("thumbnails")
    fun createThumbnails(@RequestParam("path") path: String, @RequestParam(value = "regenerate", defaultValue = "false") regenerate: Boolean): Int {
        return service.createThumbnails(path, regenerate)
    }

    @GetMapping("image")
    fun getImage(@RequestParam("path") path: String) =
            service.getImageData(path)
                    ?.let { createResponse(it) }
                    ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find image for path $path")

    private fun createResponse(image: ImageData): ResponseEntity<InputStreamResource> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.parseMediaType(image.mimeType)
        headers.contentLength = image.size
        headers.cacheControl = "max-age=37739520, public"
        val body = InputStreamResource(image.data)
        return ResponseEntity(body, headers, HttpStatus.OK)
    }

}