package com.canceledsystems.cloudimageviewer.web

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import java.net.URI

@Component
@ConfigurationProperties("security")
class SecurityProperties {

    lateinit var logoutSuccessUrl: URI
}