package com.canceledsystems.cloudimageviewer.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class MainController {

    @RequestMapping("")
    fun loggedOut() = "home"

}