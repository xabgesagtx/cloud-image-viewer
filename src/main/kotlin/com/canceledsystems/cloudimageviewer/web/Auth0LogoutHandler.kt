package com.canceledsystems.cloudimageviewer.web

import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder
import java.io.IOException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class Auth0LogoutHandler(
    private val securityProperties: SecurityProperties,
    private val clientRegistrationRepository: ClientRegistrationRepository
) :
    SecurityContextLogoutHandler() {
    override fun logout(
        httpServletRequest: HttpServletRequest?, httpServletResponse: HttpServletResponse,
        authentication: Authentication
    ) {
        super.logout(httpServletRequest, httpServletResponse, authentication)

        val issuer = getClientRegistration().providerDetails.configurationMetadata["issuer"] as String?
        val clientId = getClientRegistration().clientId
        val returnTo = securityProperties.logoutSuccessUrl
        val logoutUrl = UriComponentsBuilder
            .fromHttpUrl(issuer + "v2/logout?client_id={clientId}&returnTo={returnTo}")
            .encode()
            .buildAndExpand(clientId, returnTo)
            .toUriString()
        try {
            httpServletResponse.sendRedirect(logoutUrl)
        } catch (ioe: IOException) {
            // Handle or log error redirecting to logout URL
        }
    }

    private fun getClientRegistration(): ClientRegistration {
        return clientRegistrationRepository.findByRegistrationId("auth0")
    }
}