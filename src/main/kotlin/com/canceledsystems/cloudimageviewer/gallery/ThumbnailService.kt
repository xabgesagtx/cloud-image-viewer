package com.canceledsystems.cloudimageviewer.gallery

import com.canceledsystems.cloudimageviewer.files.FileData
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.IOException
import javax.imageio.ImageIO
import kotlin.math.min


@Service
class ThumbnailService(private val properties: ThumbnailProperties) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun generate(file: FileData): ByteArray? {
        return try {
            val image = ImageIO.read(file.data)
            val scaledImage = scaleImage(image)
            return getBinaryData(scaledImage, file.mimeType)
        } catch(e: IOException) {
            log.info("Could not create thumbnail for ${file.fullPath}")
            null
        } finally {
            file.data.close()
        }
    }

    private fun scaleImage(image: BufferedImage): Image {
        val (width, height) = calcDimensions(image.width, image.height, properties.maxWidth, properties.maxHeight)
        return image.getScaledInstance(width, height, BufferedImage.SCALE_SMOOTH)
    }

    internal fun calcDimensions(width: Int, height: Int, maxWidth: Int, maxHeight: Int): Pair<Int, Int> {
        val scalingFactor = min(maxWidth.toDouble() / width, maxHeight.toDouble()
                / height)
        val newWidth = (scalingFactor * width).toInt()
        val newHeight = (scalingFactor * height).toInt()
        return Pair(newWidth, newHeight)
    }

    private fun getBinaryData(image: Image, mimeType: String): ByteArray {
        ByteArrayOutputStream().use {
            val bufferedImage = BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB)
            bufferedImage.createGraphics().drawImage(image, 0, 0, null)
            ImageIO.write(bufferedImage, mimeType.substringAfter("/"), it)
            return it.toByteArray()
        }

    }
}