package com.canceledsystems.cloudimageviewer.gallery

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("thumbnails")
class ThumbnailProperties {
    var maxWidth: Int = 300
    var maxHeight: Int = 200
}