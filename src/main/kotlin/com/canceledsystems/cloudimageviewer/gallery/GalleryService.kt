package com.canceledsystems.cloudimageviewer.gallery

import com.canceledsystems.cloudimageviewer.files.FileData
import com.canceledsystems.cloudimageviewer.files.FileRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class GalleryService(private val fileRepository: FileRepository,
                     private val thumbnailService: ThumbnailService) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun list(path: String): GalleryListing {
        val files = fileRepository.list(path)
        val defaultFolders = defaultFolders(path)
        val folders = defaultFolders + files.asSequence()
                .filter { it.directory }
                .filter { it.name != "thumbs" }
                .map { GalleryFolder(it.name, it.fullPath) }
                .toList()
        val images = files.asSequence()
                .filter { it.image }
                .map { GalleryImage(it.name, it.fullPath, thumbnailPath(it.basePath, it.name)) }
                .toList()

        return GalleryListing(folders, images)
    }

    fun getImageData(path: String): ImageData? {
        return fileRepository.getData(path)
                ?.takeIf { it.image }
                ?.let { ImageData(path, it.mimeType, it.size, it.data) }
    }

    private fun defaultFolders(path: String): List<GalleryFolder> {
        return when {
            path.isBlank() -> {
                listOf()
            }
            path.count { it == '/' } < 2 -> {
                listOf(GalleryFolder("..", ""))
            }
            else -> {
                val trimmedPath = path.removeSuffix("/")
                val parentPath = trimmedPath.substringBeforeLast("/") + "/"
                listOf(GalleryFolder("..", parentPath))
            }
        }
    }

    fun createThumbnails(path: String, regenerate: Boolean): Int {
        val count = fileRepository.list(path)
                .asSequence()
                .filter { it.image }
                .filter { if (regenerate) true else !fileRepository.exists(thumbnailPath(it.basePath, it.name)) }
                .onEach { log.info("Creating thumbnail for ${it.name}") }
                .mapNotNull { fileRepository.getData(it.fullPath) }
                .mapNotNull { generateThumbnail(it) }
                .count()
        log.info("Created $count thumbnails")
        return count
    }

    private fun generateThumbnail(file: FileData): String? {
        return thumbnailService.generate(file)
                ?.let {
                    val fullPath = thumbnailPath(file.basePath, file.name)
                    fileRepository.save(fullPath, it, file.mimeType)
                    fullPath
                }
    }

    private fun thumbnailPath(basePath: String, fileName: String): String {
        return if (basePath.isBlank()) {
            "thumbs/$fileName"
        } else {
            "${basePath}/thumbs/$fileName"
        }
    }

}