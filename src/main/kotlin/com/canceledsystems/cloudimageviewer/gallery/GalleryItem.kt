package com.canceledsystems.cloudimageviewer.gallery

interface GalleryItem {

    val directory: Boolean
    val name: String
    val fullPath: String

}