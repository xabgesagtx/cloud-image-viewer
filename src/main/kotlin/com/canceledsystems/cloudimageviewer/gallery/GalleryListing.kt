package com.canceledsystems.cloudimageviewer.gallery

data class GalleryListing(val folders: List<GalleryFolder>,
                          val images: List<GalleryImage>)