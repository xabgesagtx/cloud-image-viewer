package com.canceledsystems.cloudimageviewer.gallery

data class GalleryFolder(override val name: String,
                         override val fullPath: String): GalleryItem {
    override val directory = true
}