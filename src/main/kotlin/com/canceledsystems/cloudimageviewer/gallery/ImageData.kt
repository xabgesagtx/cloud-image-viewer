package com.canceledsystems.cloudimageviewer.gallery

import java.io.InputStream

class ImageData(val fullPath: String,
                val mimeType: String,
                val size: Long,
                val data: InputStream)