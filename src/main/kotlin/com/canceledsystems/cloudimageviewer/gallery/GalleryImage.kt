package com.canceledsystems.cloudimageviewer.gallery

data class GalleryImage(override val name: String,
                        override val fullPath: String,
                        val thumbnailPath: String) : GalleryItem {
    override val directory = false
}