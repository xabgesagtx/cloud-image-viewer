import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage

plugins {
	id("org.springframework.boot") version "2.5.7"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.31"
	kotlin("plugin.spring") version "1.5.31"
	id("jacoco")
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
	implementation("nz.net.ultraq.thymeleaf:thymeleaf-layout-dialect")
	implementation("org.thymeleaf.extras:thymeleaf-extras-springsecurity5")
	runtimeOnly("org.webjars:webjars-locator-core")
	runtimeOnly("org.webjars:bootstrap:4.5.3")
	runtimeOnly("org.webjars:font-awesome:5.15.1")
	runtimeOnly("org.webjars:node-snackbar:0.1.16")
	runtimeOnly("org.webjars:simple-lightbox:2.1.0")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	implementation("software.amazon.awssdk:s3:2.15.54")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("io.micrometer:micrometer-registry-prometheus")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
	testImplementation("org.testcontainers:junit-jupiter:1.16.2")
	testImplementation("org.testcontainers:localstack:1.16.2")
	testImplementation("com.amazonaws:aws-java-sdk-s3:1.11.914") // necessary for localstack


	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
	docker {
		publishRegistry {
			username = project.properties["dockerUsername"]?.toString()
			password = project.properties["dockerPassword"]?.toString()
		}
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}
