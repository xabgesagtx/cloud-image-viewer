# Cloud Image Viewer


[![pipeline status](https://gitlab.com/xabgesagtx/cloud-image-viewer/badges/master/pipeline.svg)](https://gitlab.com/xabgesagtx/cloud-image-viewer/commits/master) [![coverage report](https://gitlab.com/xabgesagtx/cloud-image-viewer/badges/master/coverage.svg)](https://gitlab.com/xabgesagtx/cloud-image-viewer/commits/master)

![Cloud Image Viewer](screenshots/screenshot.png "Cloud Image Viewer screenshot")

Provides an image gallery view on your images stored in S3 - secured by OAuth2.

Features:
* image gallery view of an S3 bucket
* thumbnail generation on demand and per folder
* configurable OAuth2 based authentication

Technologies used:
* spring boot based application
* written in kotlin
* using spring security for authentication
* and amazon SDK for S3 access

## Dependencies

* java 8 for building
* docker or java 8 for running
* S3 bucket for image storage
* OAuth2 identity provider for authentication

## Build

Cloud image viewer comes with a gradle wrapper to build the application as jar

```
./gradlew build
```

## Configuration

As cloud image viewer depends on external systems to provide authentication and the images. There is some configuration needed.

The easiest way for a spring boot application is to create an application.yml

```
s3:
  key: KEY_ID
  secret: KEY_SECRET
  bucket: BUCKET
  region: REGION
  endpoint: S3_ENDPOINT
spring:
  security:
    oauth2:
      client:
        registration:
          keycloak:
            client-id: OAUTH_CLIENT_ID
            client-secret: OAUTH_CLIENT_SECRET
            authorization-grant-type: authorization_code
            redirect-uri: '{baseUrl}/login/oauth2/code/{registrationId}'
            scope:
              - openid
              - profile
              - email
              - cloudimageviewer
        provider:
          keycloak:
            issuer-uri: YOUR_OAUTH_ISSUER_URI
            user-name-attribute: preferred_username

security:
  logoutSuccessUrl: URL_TO_REDIRECT_CLIENT_AFTER_LOGOUT
```

### S3

All properties here are specific to the project and defined in the class `S3Properties`. The properties should make you able to run this project with AWS S3, Digital Ocean Spaces or any other S3 compliant API.

### OAuth2

The configuration options outlined here are just an example configuration. More details can be found in the [spring security documentation](https://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#oauth2login-sample-application-config).

It's important to note that there has to be the scope `cloudimageviewer` provided for the user that is allowed to access the image gallery.

## Development

To run in development you have provide all the configuration options outlined under configuration.

## Run in production

### Jar

You can either build the application yourself and run the jar:

```
java -jar build/libs/cloud-image-viewer.jar
```

Make sure you put the application.yml with the required configurations in the folder where you run this command.

### Docker

Alternatively, you can use the docker image from gitlab:

```
docker run -p "8080:8080" -v PATH_TO_APPLICATION_YML:/application.yml registry.gitlab.com/xabgesagtx/cloud-image-viewer
```